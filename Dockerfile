FROM ubuntu:16.04
RUN apt-get update
RUN apt-get install squid -y && apt-get install wget -y
ENTRYPOINT service squid start && bash
